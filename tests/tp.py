# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return "S" * n + "0"


def S(n: str) -> str:
    #return len(n) * 'S'  + '0'
    return f"S{n}"

def addition(a: str, b: str) -> str:
    return a.replace('0', '') + b.replace('0', '') + '0'
    

def multiplication(a: str, b: str) -> str:
    return (len(a.replace('0', '')) * len(b.replace('0', ''))) * 'S' + '0'


def facto_ite(n: int) -> int:
    res = 1
    for i in range(1, n + 1):
        res *= i
    return res


def facto_rec(n: int) -> int:
    return 1 if n == 0 else n * facto_ite(n - 1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n: int) -> int:
    a, b, c = 0, 1, 0
    for i in range(n):
        c = a + b
        a = b
        b = c
    return a


def golden_phi(n: int) -> float:
    if n == 1:
        return 1  # Premier ratio approximatif (F(2) / F(1) = 1 / 1)
    elif n == 2:
        return 2  # Deuxième ratio approximatif (F(3) / F(2) = 2 / 1)
    
    a, b = 1, 1  # Initialisation des deux premiers nombres de Fibonacci
    for _ in range(n - 1):
        a, b = b, a + b
    
    return b / a

def sqrt5(n: int) -> float:
    # Méthode de Newton pour approximer la racine carrée de 5
    return 



def pow(a: float, n: int) -> float:
    return a ** n

